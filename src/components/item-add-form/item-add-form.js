import  React, {Component} from 'react';
import './item-add-form.css';

export default class ItemAddForm extends Component{
    state ={
        label:''
    }
    onLableChange = (e)=>{
        this.setState({
            label: e.target.value
        })
        console.log(e.target.value);
    };
    onSubmit = (e)=>{
        e.preventDefault();
        this.props.onItemAdded(this.state.label);
        this.setState({
            label: ''
        });
    };


    render(){
        return (
        <form className="item-add-form d-flex"
            onSubmit={this.onSubmit}>

            <input  type="text" 
                    className="form-control" 
                    onChange={this.onLableChange}
                    value={this.state.label}
                    placeholder="What need to be done"/>
            <button type="button"
                className="btn btn-outline-secondary"
                onClick={this.onSubmit}
            >Add_item
            </button>
        </form>
        )
    }
}